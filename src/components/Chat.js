import React from 'react';
import MessageList from '../messages-list/MessageList';
import './Chat.css';

function Chat({url}) {
  return (
    <div className="Chat">
      <MessageList url={url} />      
    </div>
  );
}

export default Chat;