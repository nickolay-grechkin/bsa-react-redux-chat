import React from 'react';
import Typography from '@material-ui/core/Typography';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import FavoriteIcon from '@material-ui/icons/Favorite';
import IconButton from '@material-ui/core/IconButton';
import dateFormat from 'dateformat';
import './Chat.css'

class Message extends React.Component {
    render () {
        const { avatar, user, text, createdAt, id, handleLikeMessage } = this.props;
        return (
            <div className="message">
                <CardContent className="message">                                     
                    <Avatar className="message-user-avatar" src={avatar}/>
                    <Typography className="message-user-name" color="textSecondary" gutterBottom>                                       
                        {user}
                    </Typography> 
                    <Typography variant="h5" component="h2" className="message-text">
                        {text}
                    </Typography>
                    <Typography align="right" variant="body2" className="message-time">
                        {dateFormat(createdAt, 'UTC: HH:MM')}
                    </Typography>
                    <IconButton className="message-liked" onClick={() => {handleLikeMessage(id)}}>
                        <FavoriteIcon className={this.props.message.isLiked ? "message-liked":"message-like"} />
                    </IconButton>
                </CardContent>
            </div>
        );
    }
}

export default Message