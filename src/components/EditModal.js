import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';

class EditModal extends React.Component {
    onChange = (e) => {
        this.setState ({
            "text": e.target.value,
        });
    }
    render() {
        const {editModal, handleCloseModal, handleUpdateMessage } = this.props;
        return (
            <div className="edit-message-modal">
                <Dialog open={editModal}  aria-labelledby="form-dialog-title" className="modal-shown">
                    <DialogTitle id="form-dialog-title">Update message</DialogTitle>
                    <DialogContent modal>
                        <DialogContentText>
                        </DialogContentText>
                        <TextareaAutosize className="edit-message-input"
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Email Address"
                            onChange = {(e) => this.onChange(e)}
                            type="email"
                        />
                        </DialogContent>
                        <DialogActions>
                            <Button className="edit-message-button" color="primary" onClick = {() => handleCloseModal()}>
                                Close
                            </Button>
                            <Button className="edit-message-close" color="primary" onClick = {() => handleUpdateMessage(this.state.text)}>
                                Update
                            </Button>
                        </DialogActions>
                </Dialog>
            </div>
        )
    }
}

export default EditModal;