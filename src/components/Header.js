import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Badge from '@material-ui/core/Badge';
import MailIcon from '@material-ui/icons/Mail';
import GroupIcon from '@material-ui/icons/Group';
import Icon from '@material-ui/core/Icon';
import dateFormat from 'dateformat';

function Header({ messages }){
  var messageTime = " ";
  const usersNames = messages.map(function (message) {
    return message.user;
    })
  if (messages.length !== 0) {
    messageTime = messages[messages.length-1].createdAt;
  }
  const uniqueUsers = Array.from(new Set(usersNames));
    return (
      <div className="header">
        <AppBar position="fixed">
          <Toolbar>
            <Typography variant="h6" className="header-title">
              React Chat
            </Typography>
            <Badge badgeContent={messages.length} className="header-messages-count" color="secondary">
              <MailIcon />
            </Badge>
            <Badge badgeContent={uniqueUsers.length} className="header-users-count" color="secondary">
              <GroupIcon />
            </Badge>
            <Badge className="header-last-message-date" color="secondary">
              {dateFormat(messageTime, 'UTC:dd.mm.yyyy HH:MM')}                    
            </Badge>
            <Icon className="fa fa-plus-circle" color="primary" />
              </Toolbar>
            </AppBar>
      </div>
    );
}

export default Header