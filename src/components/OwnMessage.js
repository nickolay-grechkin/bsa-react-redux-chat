import React from 'react';
import Typography from '@material-ui/core/Typography';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import dateFormat from 'dateformat';


class OwnMessage extends React.Component {
    render() {
        const {handleDeleteMessage, handleOpenModal } = this.props;
        return (
            <div className="own-message">
                <CardContent >                                   
                    <Avatar className="own-message-avatar" color="textSecondary" src={this.props.message.avatar}/>
                    <IconButton color="secondary" aria-label="delete" className="message-delete" onClick={() => handleDeleteMessage(this.props.message.id)}  >                                       
                        <HighlightOffIcon />
                    </IconButton> 
                    <IconButton color="secondary" aria-label="delete" className="message-edit" onClick={() => handleOpenModal(this.props.message.id)}>                                       
                        <EditIcon />
                    </IconButton> 
                    <Typography variant="h5" component="h2" className="message-text">
                        {this.props.message.text}
                    </Typography>
                    <Typography align="right" variant="body2" className="message-time">
                        {dateFormat(this.props.message.createdAt, 'UTC: HH:MM')}
                    </Typography>  
                </CardContent>
            </div>
        )
    }
}

export default OwnMessage