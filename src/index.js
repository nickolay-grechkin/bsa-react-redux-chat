import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './store';
import './index.css';

import Chat from './components/Chat';

const store = configureStore();

render(
    <Provider store={store}>
        <Chat url='https://edikdolynskyi.github.io/react_sources/messages.json'/>
    </Provider>,
    document.getElementById('root')
);
