import { combineReducers } from 'redux';
import  chat  from '../messages-list/reducer';

const rootReducer = combineReducers({
    chat
});

export default rootReducer;
