import { v4 as uuidv4 } from 'uuid';

const initialState = {
    messages:[],
    preloader:false,
    editModal: false,
    messageId: "",
    hasError:false
}

export default function (state = initialState, action) {
    switch (action.type) {
        
        case 'MESSAGES_IS_LOADING': {
            return {
                preloader: true
            };
        }

        case 'MESSAGES_HAS_ERRORED': {
            return {
                hasError: true
            };
        }

        case 'MESSAGES_FETCH_DATA_SUCCESS': {
            return {
                messages: action.messages,
                preloader: false,
                editModal: false,
            };
        }

        case 'RELOAD':
            return {
                ...state,
                preloader: !state.preloader
            };

            
        case 'SET_MESSAGE_ID': {
            const { id } = action.payload
            return {
                ...state,
                messageId: id
            }
        }

        case 'LIKE':
            const { id } = action.payload;
            const message = state.messages.find(msg => msg.id === id);
            const index = state.messages.indexOf(message);
            state.messages[index].isLiked = !state.messages[index].isLiked;
            return {
                ...state
            };
        
        case 'ADD_MESSAGE': {
            const { data } = action.payload;
            const newMessage = {"id": uuidv4(),
            "userId": "1cfa92a7-3c6e-4ca9-80be-279800680397",
            "text": data.message.text,
            "createdAt": new Date().toISOString(),
            "editedAt": new Date()
            }
            state.messages.push(newMessage);
            return {
                ...state
            }
        }

        case 'DELETE_MESSAGE': {
            const { id } = action.payload;
            const message = state.messages.find(msg => msg.id === id);
            const index = state.messages.indexOf(message);
            state.messages.splice(index, 1);
            return {
                ...state
            }
        }

        case 'UPDATE_MESSAGE': {
            const { data } = action.payload;
            const message = state.messages.find(msg => msg.id === state.messageId);
            const index = state.messages.indexOf(message);
            state.messages[index].text = data;
            return {
                ...state
            }
        }
        
        case 'SHOW_MODAL':
            return {
                ...state,
                editModal: true
            }

        case 'HIDE_MODAL':
            return {
                ...state,
                editModal: false
        }

        default:
            return state;
    }
}