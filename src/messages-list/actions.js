import * as action from "./actiontypes";

export function messagesHasErrored(bool) {
    return {
        type: action.MESSAGES_HAS_ERRORED,
        hasErrored: bool
    };
}

export function messagesIsLoading(bool) {
    return {
        type: action.MESSAGES_IS_LOADING,
        isLoading: bool
    };
}

export function messagesFetchDataSuccess(messages) {
    return {
        type: action.MESSAGES_FETCH_DATA_SUCCESS,
        messages
    };
}

export const like = id => ({
    type: action.LIKE,
    payload: {
        id
    }
});

export const reload = () => ({
    type: action.RELOAD
});

export const addMessage = data => ({
    type: action.ADD_MESSAGE,
    payload: {
        data
    }
})

export const showModal = () => ({
    type: action.SHOW_MODAL
})

export const hideModal = () => ({
    type: action.HIDE_MODAL
})

export const updateMessage = (data) => ({
    type: action.UPDATE_MESSAGE,
    payload: {
        data
    }
})

export const setMessageId = (id) => ({
    type: action.SET_MESSAGE_ID,
    payload: {
        id
    }
})

export const deleteMessage = (id) => ({
    type: action.DELETE_MESSAGE,
    payload: {
        id
    }
})

export function messagesFetchData(url) {
    return (dispatch) => {
        dispatch(messagesIsLoading(true));

        fetch(url)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(messagesIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((messages) => dispatch(messagesFetchDataSuccess(messages)))
            .catch(() => dispatch(messagesHasErrored(true)));
    };
}
