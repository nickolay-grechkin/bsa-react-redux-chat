import React, { Component } from 'react';
import { connect } from 'react-redux';
import Container from '@material-ui/core/Container';
import Message from '../components/Message'
import Header from '../components/Header'
import OwnMessage from '../components/OwnMessage'
import MessageInput from '../components/MessageInput'
import Preloader from '../components/Preloader';
import EditModal from '../components/EditModal';
import * as actions from './actions';

class MessageList extends Component {
    constructor(props){
        super(props);
        this.handleEnter = this.handleEnter.bind(this);
        this.handleOpenModal = this.handleOpenModal.bind(this);
        this.handleDeleteMessage = this.handleDeleteMessage.bind(this);
        this.handleLikeMessage = this.handleLikeMessage.bind(this);
        this.handleAddMessage = this.handleAddMessage.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.handleUpdateMessage = this.handleUpdateMessage.bind(this);
    }

    componentDidMount() {
        this.props.messagesFetchData(this.props.url);
        document.addEventListener('keydown',this.handleEnter);
    }

    componentWillUnmount() {
        document.removeEventListener('keydown', this.handleEnter);
    }

    handleOpenModal (id) { 
        this.props.showModal();
        this.props.setMessageId(id);
    }

    handleEnter(event) {
        if (event.keyCode === 38 && this.props.messages[this.props.messages.length - 1].userId === "1cfa92a7-3c6e-4ca9-80be-279800680397") {
            this.handleOpenModal(this.props.messages[this.props.messages.length - 1].id);
        }
    }

    handleLikeMessage(id) {
        this.props.like(id);
        this.reloadPage();
    }

    reloadPage() {
        this.props.reload();
        this.props.reload();
    }

    handleAddMessage(data) {
        this.props.addMessage(data);
        this.reloadPage();
    }

    handleCloseModal() {
        this.props.hideModal();
    }

    handleUpdateMessage(data) {
        this.props.updateMessage(data);
        this.handleCloseModal();
        this.reloadPage();
    }

    handleDeleteMessage(id) {
        this.props.deleteMessage(id);
        this.reloadPage()
    }

    getCurrentUserId() {
        return "1cfa92a7-3c6e-4ca9-80be-279800680397";
    }

    render() {
        if (this.props.preloader) {
            return <Preloader/>;
        }

        if (this.props.hasError) {
            return (
                <div className="error">
                    <h3>Something went wrong!</h3>
                </div>
            )
        }

        return (
            <div className="chat-main-window">
                <Header messages={this.props.messages} />
                {this.props.messages.map((message) => {
                    if (message.userId === this.getCurrentUserId()) {
                        return (
                            <Container key={message.id}>
                            <OwnMessage
                                message={message}
                                id={message.id} 
                                avatar={message.avatar}
                                user={message.user}
                                text={message.text}
                                createdAt={message.createdAt}
                                isLiked={message.isLiked} 
                                handleOpenModal = {this.handleOpenModal}
                                handleDeleteMessage = {this.handleDeleteMessage} />
                            </Container>
                        );
                    }
                    else {
                        return (
                            <Container key={message.id}>
                                <Message
                                    message={message}
                                    id={message.id} 
                                    avatar={message.avatar}
                                    user={message.user}
                                    text={message.text}
                                    createdAt={message.createdAt}
                                    isLiked={message.isLiked} 
                                    handleLikeMessage = {this.handleLikeMessage} />
                            </Container>
                        )
                    }
                })}
                <MessageInput 
                handleAddMessage = {this.handleAddMessage} />
                <EditModal 
                editModal={this.props.editModal} 
                handleCloseModal={this.handleCloseModal}
                handleUpdateMessage={this.handleUpdateMessage}
                id={this.props.messageId} />
            </div> 
        );
    }
}

const mapStateToProps = (state) => {
    return {
        messages: state.chat.messages,
        preloader: state.chat.preloader,
        editModal: state.chat.editModal,
        messageId: state.chat.messageId,
        hasError: state.chat.hasError
    };
};

const mapDispatchToProps =  {
    ...actions  
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageList);
